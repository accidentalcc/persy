use crate::{
    error::{IndexChangeError, PERes, PIRes, TimeoutError},
    id::{IndexId, RecRef, SegmentId},
    index::{
        config::{IndexTypeInternal, Indexes, ValueMode},
        entries_container::{
            add_entry, add_value, eapplier, get_changes, resolve_range, resolve_values, Change, Changes,
            EntriesContainer, ValueChange, ValueContainer,
        },
        keeper::{map_read_err, IndexKeeper, IndexModify},
        serialization::{deserialize, serialize},
        tree::{
            nodes::{compare, Node, NodeRef, Nodes, Value},
            IndexApply, KeyChanges, ValueChange as TreeValue,
        },
    },
    transaction::{index_locks::IndexDataLocks, tx_impl::TransactionImpl},
    PersyImpl,
};

use std::{
    cmp::Ordering,
    collections::{
        btree_map::{BTreeMap, Entry as BTreeEntry},
        hash_map::{Entry, HashMap},
    },
    ops::RangeBounds,
    rc::Rc,
    vec::IntoIter,
};

pub(crate) fn apply_to_index<'a, K, V>(
    store: ExternalRefs<'a>,
    index_id: &IndexId,
    keys: &[(K, Changes)],
    values: &[V],
) -> PIRes<()>
where
    K: IndexTypeInternal,
    V: IndexTypeInternal,
{
    let changes: Vec<_> = keys
        .iter()
        .map(|(k, c)| {
            let vals: Vec<_> = c
                .changes
                .iter()
                .map(|ch| match *ch {
                    Change::Add(pos) => TreeValue::Add(values[pos].clone()),
                    Change::Remove(pos) => TreeValue::Remove(pos.map(|p| values[p].clone())),
                })
                .collect();
            KeyChanges::new(k.clone(), vals)
        })
        .collect();
    let mut index = Indexes::get_index_keeper_tx::<K, V>(store, index_id)?;
    index.apply(&changes)?;
    index.update_changed()?;
    Ok(())
}

pub struct IndexTransactionKeeper {
    indexex_changes: BTreeMap<IndexId, (EntriesContainer, ValueContainer)>,
}

impl IndexTransactionKeeper {
    pub fn new() -> IndexTransactionKeeper {
        IndexTransactionKeeper {
            indexex_changes: BTreeMap::new(),
        }
    }

    pub fn put<K, V>(&mut self, index: IndexId, k: K, v: V)
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
    {
        match self.indexex_changes.entry(index) {
            BTreeEntry::Occupied(ref mut o) => {
                let (entries, values) = o.get_mut();
                let pos = add_value(values, v);
                add_entry(entries, k, Change::Add(pos));
            }
            BTreeEntry::Vacant(va) => {
                let mut values = V::new_values();
                let mut keys = K::new_entries();
                let pos = add_value(&mut values, v);
                add_entry(&mut keys, k, Change::Add(pos));
                va.insert((keys, values));
            }
        }
    }

    pub fn remove<K, V>(&mut self, index: IndexId, k: K, v: Option<V>)
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
    {
        match self.indexex_changes.entry(index) {
            BTreeEntry::Occupied(ref mut o) => {
                let pos = v.map(|val| add_value(&mut o.get_mut().1, val));
                add_entry(&mut o.get_mut().0, k, Change::Remove(pos));
            }
            BTreeEntry::Vacant(va) => {
                let mut values = V::new_values();
                let mut keys = K::new_entries();
                let pos = v.map(|val| add_value(&mut values, val));
                add_entry(&mut keys, k, Change::Remove(pos));
                va.insert((keys, values));
            }
        }
    }

    pub fn get_changes<K, V>(&self, index: IndexId, k: &K) -> Option<Vec<ValueChange<V>>>
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
    {
        self.indexex_changes
            .get(&index)
            .map(|o| get_changes(&o.0, k).map(|c| resolve_values(&o.1, c)))
            .and_then(std::convert::identity)
    }

    pub fn apply_changes<K, V>(
        &self,
        index_id: IndexId,
        index_name: &str,
        vm: ValueMode,
        k: &K,
        pers: Option<Value<V>>,
    ) -> Result<Option<Value<V>>, IndexChangeError>
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
    {
        let mut result = pers;
        if let Some(key_changes) = self.get_changes::<K, V>(index_id, k) {
            for change in key_changes {
                result = match change {
                    ValueChange::Add(add_value) => Some(if let Some(s_result) = result {
                        match s_result {
                            Value::Single(v) => match vm {
                                ValueMode::Replace => Value::Single(add_value),
                                ValueMode::Exclusive => {
                                    if compare(&v, &add_value) == Ordering::Equal {
                                        Value::Single(v)
                                    } else {
                                        return Err(IndexChangeError::IndexDuplicateKey(
                                            index_name.to_string(),
                                            format!("{}", k),
                                        ));
                                    }
                                }
                                ValueMode::Cluster => match compare(&v, &add_value) {
                                    Ordering::Equal => Value::Single(v),
                                    Ordering::Less => Value::Cluster(vec![v, add_value]),
                                    Ordering::Greater => Value::Cluster(vec![add_value, v]),
                                },
                            },
                            Value::Cluster(mut values) => {
                                if let Err(pos) = values.binary_search_by(|x| compare(x, &add_value)) {
                                    values.insert(pos, add_value);
                                }
                                Value::Cluster(values)
                            }
                        }
                    } else {
                        Value::Single(add_value)
                    }),
                    ValueChange::Remove(rv) => rv.and_then(|remove_value| {
                        result.and_then(|s_result| match s_result {
                            Value::Single(v) => {
                                if compare(&v, &remove_value) == Ordering::Equal {
                                    None
                                } else {
                                    Some(Value::Single(v))
                                }
                            }
                            Value::Cluster(mut values) => {
                                if let Ok(pos) = values.binary_search_by(|x| compare(x, &remove_value)) {
                                    values.remove(pos);
                                }
                                Some(if values.len() == 1 {
                                    Value::Single(values.pop().unwrap())
                                } else {
                                    Value::Cluster(values)
                                })
                            }
                        })
                    }),
                };
            }
        }
        Ok(result)
    }

    pub(crate) fn apply(
        &self,
        operations: &mut IndexDataLocks,
        persy: &PersyImpl,
        tx: &mut TransactionImpl,
    ) -> PIRes<()> {
        for (index, (keys, values)) in &self.indexex_changes {
            let store = ExternalRefs::write(persy, tx, operations);
            eapplier(keys, values, index, store)?;
        }
        Ok(())
    }

    pub fn range<K, V, R>(&self, index: IndexId, range: R) -> Option<IntoIter<K>>
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
        R: RangeBounds<K>,
    {
        self.indexex_changes.get(&index).map(|x| resolve_range(&x.0, range))
    }

    pub fn changed_indexes(&self) -> Vec<IndexId> {
        self.indexex_changes.keys().cloned().collect()
    }
    pub fn remove_changes(&mut self, index_id: &IndexId) {
        self.indexex_changes.remove(index_id);
    }
}

struct LockData {
    version: u16,
    counter: u32,
}

pub struct ExternalRefs<'a> {
    pub(crate) persy: &'a PersyImpl,
    pub(crate) tx: &'a mut TransactionImpl,
    operations: Option<&'a mut IndexDataLocks>,
}
impl<'a> ExternalRefs<'a> {
    pub(crate) fn write(persy: &'a PersyImpl, tx: &'a mut TransactionImpl, operations: &'a mut IndexDataLocks) -> Self {
        Self {
            persy,
            tx,
            operations: Some(operations),
        }
    }

    pub fn lock_record(&mut self, segment_id: SegmentId, id: &RecRef, version: u16) -> Result<bool, TimeoutError> {
        self.operations
            .as_mut()
            .unwrap()
            .lock_record(self.persy, segment_id, id, version)
    }

    pub fn unchecked_lock_record(&mut self, segment_id: SegmentId, id: &RecRef) -> Result<bool, TimeoutError> {
        self.operations
            .as_mut()
            .unwrap()
            .unchecked_lock_record(self.persy, segment_id, id)
    }

    pub fn unlock_record(&mut self, segment: SegmentId, id: &RecRef) {
        self.operations.as_mut().unwrap().unlock_record(self.persy, segment, id)
    }
}

pub struct IndexSegmentKeeperTx<'a, K, V> {
    name: String,
    index_id: IndexId,
    root: Option<NodeRef>,
    config_version: u16,
    store: ExternalRefs<'a>,
    value_mode: ValueMode,
    changed: Option<HashMap<NodeRef, (Rc<Node<K, V>>, u16)>>,
    bottom_limit: usize,
    top_limit: usize,
    locked: HashMap<NodeRef, LockData>,
    updated_root: bool,
}

impl<'a, K: IndexTypeInternal, V: IndexTypeInternal> IndexSegmentKeeperTx<'a, K, V> {
    pub fn new(
        name: &str,
        index_id: &IndexId,
        root: Option<NodeRef>,
        config_version: u16,
        store: ExternalRefs<'a>,
        value_mode: ValueMode,
        bottom_limit: usize,
        top_limit: usize,
    ) -> IndexSegmentKeeperTx<'a, K, V> {
        IndexSegmentKeeperTx {
            name: name.to_string(),
            index_id: index_id.clone(),
            root,
            config_version,
            store,
            value_mode,
            changed: None,
            bottom_limit,
            top_limit,
            locked: HashMap::new(),
            updated_root: false,
        }
    }
    pub fn update_changed(&mut self) -> PIRes<()> {
        if let Some(m) = &self.changed {
            for (node_ref, node) in m {
                self.store.persy.update(
                    self.store.tx,
                    self.index_id.get_data_id(),
                    node_ref,
                    &serialize(&node.0),
                )?;
            }
        }
        if self.updated_root {
            Indexes::update_index_root(self.store.persy, self.store.tx, &self.index_id, self.root)?;
        }
        Ok(())
    }
}

impl<'a, K: IndexTypeInternal, V: IndexTypeInternal> IndexModify<K, V> for IndexSegmentKeeperTx<'a, K, V> {
    fn load_modify(&self, node: &NodeRef) -> PIRes<Option<(Rc<Node<K, V>>, u16)>> {
        if let Some(m) = &self.changed {
            if let Some(n) = m.get(node) {
                return Ok(Some(n.clone()));
            }
        }
        if let Some((rec, version)) = self
            .store
            .persy
            .read_tx_internal_fn(self.store.tx, self.index_id.get_data_id(), node, deserialize)
            .map_err(map_read_err)?
        {
            Ok(Some((Rc::new(rec), version)))
        } else {
            Ok(None)
        }
    }
    fn lock(&mut self, node: &NodeRef, version: u16) -> PIRes<bool> {
        if let Some(lock_data) = self.locked.get_mut(node) {
            assert!(version == lock_data.version);
            lock_data.counter += 1;
            Ok(true)
        } else if self
            .store
            .lock_record(self.index_id.get_data_id(), node, version)
            .unwrap_or(false)
        {
            self.locked.insert(*node, LockData { version, counter: 1 });
            Ok(true)
        } else {
            Ok(false)
        }
    }

    fn owned(&mut self, node_ref: &NodeRef, mut node: Rc<Node<K, V>>) -> (Node<K, V>, bool) {
        debug_assert!(self.locked.contains_key(node_ref));
        let mut removed = false;
        if let Some(changed) = &mut self.changed {
            removed = changed.remove(node_ref).is_some();
        }
        Rc::make_mut(&mut node);
        (Rc::try_unwrap(node).ok().unwrap(), removed)
    }

    fn unlock(&mut self, node: &NodeRef) -> bool {
        if let Entry::Occupied(mut x) = self.locked.entry(*node) {
            x.get_mut().counter -= 1;
            if x.get().counter == 0 {
                x.remove();
                self.store.unlock_record(self.index_id.get_data_id(), node);
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    fn get_root_refresh(&mut self) -> PIRes<Option<NodeRef>> {
        if !self.updated_root {
            let (config, version) = Indexes::get_index_tx(self.store.persy, self.store.tx, &self.index_id)?;
            self.root = config.get_root();
            self.config_version = version;
        }
        Ok(self.root)
    }
    fn lock_config(&mut self) -> PIRes<bool> {
        let config_id = Indexes::get_config_id(self.store.persy, self.store.tx, &self.index_id)?.0;
        if self.store.tx.segment_created_in_tx(self.index_id.get_meta_id()) {
            self.store
                .unchecked_lock_record(self.index_id.get_meta_id(), &config_id)?;
            self.locked.insert(
                config_id,
                LockData {
                    version: self.config_version,
                    counter: 1,
                },
            );
            Ok(true)
        } else if let Some(lock_data) = self.locked.get_mut(&config_id) {
            if self.config_version == lock_data.version {
                lock_data.counter += 1;
                Ok(true)
            } else {
                panic!("this should never happen");
            }
        } else if self
            .store
            .lock_record(self.index_id.get_meta_id(), &config_id, self.config_version)?
        {
            self.locked.insert(
                config_id,
                LockData {
                    version: self.config_version,
                    counter: 1,
                },
            );
            Ok(true)
        } else {
            let (config, version) = Indexes::get_index_tx(self.store.persy, self.store.tx, &self.index_id)?;
            self.root = config.get_root();
            self.config_version = version;
            Ok(false)
        }
    }

    fn insert(&mut self, node: Node<K, V>) -> PIRes<NodeRef> {
        let node_ref = self
            .store
            .persy
            .insert_record(self.store.tx, self.index_id.get_data_id(), &serialize(&node))?;
        self.changed
            .get_or_insert_with(HashMap::new)
            .insert(node_ref, (Rc::new(node), 1));

        self.locked.insert(node_ref, LockData { version: 1, counter: 1 });
        Ok(node_ref)
    }

    fn update(&mut self, node_ref: &NodeRef, node: Node<K, V>, version: u16) -> PIRes<()> {
        debug_assert!(self.locked.contains_key(node_ref));
        self.changed
            .get_or_insert_with(HashMap::new)
            .insert(*node_ref, (Rc::new(node), version));
        Ok(())
    }

    fn delete(&mut self, node: &NodeRef, _version: u16) -> PIRes<()> {
        debug_assert!(self.locked.contains_key(node));
        if let Some(m) = &mut self.changed {
            m.remove(node);
        }
        self.store
            .persy
            .delete(self.store.tx, self.index_id.get_data_id(), node)?;
        Ok(())
    }
    fn set_root(&mut self, root: Option<NodeRef>) -> PIRes<()> {
        self.updated_root = self.root != root;
        self.root = root;
        Ok(())
    }

    fn bottom_limit(&self) -> usize {
        self.bottom_limit
    }
    fn top_limit(&self) -> usize {
        self.top_limit
    }

    fn is_locked(&mut self, node: &NodeRef) -> PIRes<bool> {
        Ok(self.locked.contains_key(node))
    }
}

impl<'a, K: IndexTypeInternal, V: IndexTypeInternal> IndexKeeper<K, V> for IndexSegmentKeeperTx<'a, K, V> {
    fn load(&self, node: &NodeRef) -> PERes<Node<K, V>> {
        if let Some(m) = &self.changed {
            if let Some(n) = m.get(node) {
                return Ok(n.0.as_ref().clone());
            }
        }
        let (rec, _) = self
            .store
            .persy
            .read_tx_internal_fn(self.store.tx, self.index_id.get_data_id(), node, deserialize)
            .map_err(map_read_err)?
            .unwrap();
        Ok(rec)
    }
    fn load_with(&self, node: &NodeRef, _reuse: Option<Nodes<K>>) -> PERes<Node<K, V>> {
        self.load(node)
    }
    fn get_root(&self) -> PERes<Option<NodeRef>> {
        Ok(self.root)
    }
    fn value_mode(&self) -> ValueMode {
        self.value_mode.clone()
    }

    fn index_name(&self) -> &String {
        &self.name
    }
}
