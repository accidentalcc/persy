use crate::{
    id::IndexId,
    index::{
        config::IndexTypeInternal,
        tree::nodes::{PageIter, PageIterBack, Value},
    },
    persy::PersyImpl,
    snapshots::SnapshotRef,
};
use std::{cmp::Ordering, ops::Bound};

pub struct IndexRawIter<K, V> {
    index_id: IndexId,
    read_snapshot: SnapshotRef,
    iter: PageIter<K, V>,
    back: PageIterBack<K, V>,
}

impl<K, V> IndexRawIter<K, V>
where
    K: IndexTypeInternal,
    V: IndexTypeInternal,
{
    pub fn new(
        index_id: IndexId,
        read_snapshot: &SnapshotRef,
        iter: PageIter<K, V>,
        back: PageIterBack<K, V>,
    ) -> IndexRawIter<K, V> {
        IndexRawIter {
            index_id,
            read_snapshot: read_snapshot.clone(),
            iter,
            back,
        }
    }

    pub fn next(&mut self, persy_impl: &PersyImpl) -> Option<(K, Value<V>)> {
        let back_keep = self.back.iter.peek();
        if let (Some(s), Some(e)) = (self.iter.iter.peek(), back_keep) {
            if s.key.cmp(&e.key) == Ordering::Greater {
                return None;
            }
        }
        if let Some(n) = self.iter.iter.next() {
            if self.iter.iter.peek().is_none() {
                if let Ok(iter) = persy_impl.index_next(&self.index_id, &self.read_snapshot, Bound::Excluded(&n.key)) {
                    self.iter = iter;
                }
            }
            Some((n.key, n.value))
        } else {
            None
        }
    }

    pub fn next_back(&mut self, persy_impl: &PersyImpl) -> Option<(K, Value<V>)> {
        let front_keep = self.iter.iter.peek();
        if let (Some(s), Some(e)) = (self.back.iter.peek(), front_keep) {
            if s.key.cmp(&e.key) == Ordering::Less {
                return None;
            }
        }
        if let Some(n) = self.back.iter.next() {
            if self.back.iter.peek().is_none() {
                if let Ok(back) = persy_impl.index_back(&self.index_id, &self.read_snapshot, Bound::Excluded(&n.key)) {
                    self.back = back;
                }
            }
            Some((n.key, n.value))
        } else {
            None
        }
    }
}
