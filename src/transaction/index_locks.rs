use crate::{
    error::TimeoutError,
    id::{IndexId, RecRef, SegmentId},
    persy::PersyImpl,
    transaction::tx_impl::CheckRecord,
};
use std::{collections::HashSet, time::Duration};

#[derive(Clone)]
pub(crate) struct IndexDataLocks {
    locked_index_pages: HashSet<RecRef>,
    index_segments: HashSet<SegmentId>,
    timeout: Duration,
}
impl IndexDataLocks {
    pub(crate) fn new(timeout: Duration) -> Self {
        Self {
            locked_index_pages: Default::default(),
            index_segments: Default::default(),
            timeout,
        }
    }

    pub fn unchecked_lock_record(
        &mut self,
        persy: &PersyImpl,
        _segment_id: SegmentId,
        id: &RecRef,
    ) -> Result<bool, TimeoutError> {
        let address = persy.address();
        if !self.locked_index_pages.contains(id) {
            address.acquire_record_lock(id, self.timeout)?;
            self.locked_index_pages.insert(*id);
            Ok(true)
        } else {
            Ok(false)
        }
    }
    pub fn lock_record(
        &mut self,
        persy: &PersyImpl,
        segment_id: SegmentId,
        id: &RecRef,
        version: u16,
    ) -> Result<bool, TimeoutError> {
        let address = persy.address();
        let locked_page = if !self.locked_index_pages.contains(id) {
            address.acquire_record_lock(id, self.timeout)?;
            true
        } else {
            false
        };

        if address
            .check_persistent_records(&[CheckRecord::new(segment_id, *id, version)], true)
            .is_ok()
        {
            if locked_page {
                self.locked_index_pages.insert(*id);
            }
            Ok(true)
        } else {
            if locked_page {
                address.release_record_lock(id);
            }
            Ok(false)
        }
    }
    pub fn is_record_locked(&self, id: &RecRef) -> bool {
        self.locked_index_pages.contains(id)
    }
    pub fn is_segment_locked(&self, id: &SegmentId) -> bool {
        self.index_segments.contains(id)
    }
    pub fn read_lock_indexes(&mut self, persy_impl: &PersyImpl, to_lock: &[IndexId]) -> Result<(), TimeoutError> {
        let address = persy_impl.address();
        let mut index_segments = Vec::new();
        for index_id in to_lock {
            index_segments.push(index_id.get_meta_id());
            index_segments.push(index_id.get_data_id());
        }
        index_segments.sort();
        address.acquire_segments_read_lock(&index_segments, self.timeout)?;
        for segment in index_segments {
            self.index_segments.insert(segment);
        }
        Ok(())
    }

    pub fn unlock_record(&mut self, persy: &PersyImpl, _segment: SegmentId, id: &RecRef) {
        let address = persy.address();
        let removed = self.locked_index_pages.remove(id);
        assert!(removed);
        address.release_record_lock(id);
    }

    pub fn extract(mut self, records: &[RecRef]) -> (HashSet<RecRef>, HashSet<SegmentId>) {
        for rec in records {
            self.locked_index_pages.remove(rec);
        }
        (self.locked_index_pages, self.index_segments)
    }
}
