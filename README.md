# Persy
[![build status](https://gitlab.com/tglman/persy/badges/master/pipeline.svg)](https://gitlab.com/tglman/persy/commits/master)
[![coverage report](https://codecov.io/gl/tglman/persy/branch/master/graph/badge.svg?token=4Hqx7T9z1w)](https://codecov.io/gl/tglman/persy)

[Persy](https://persy.rs) is a transactional storage engine written in rust.

## Install

Add it as dependency of your project:

```toml
[dependencies]
persy="1.5"
```

## Example

Create a new persy file save some data in it and scan it.

```rust
use persy::{Persy,Config};
//...
Persy::create("./open.persy")?;
let persy = Persy::open("./open.persy",Config::new())?;
let mut tx = persy.begin()?;
tx.create_segment("seg")?;
let data = vec![1;20];
tx.insert("seg", &data)?;
let prepared = tx.prepare()?;
prepared.commit()?;
for (_id, content) in persy.scan("seg")? {
    assert_eq!(content[0], 1);
    //....
}
```

## Docs

Check the [get started](https://persy.rs/posts/getting-started.html) and the [APIs](https://docs.rs/persy/)
look for all the version on [crates.io](https://crates.io/crates/persy)  

## Compile The Source

Checkout the source code:

```
git clone https://gitlab.com/tglman/persy.git
```

Compile and Test

```
cargo test
```

## Contacts

Follow Persy on [Mastodon](https://fosstodon.org/@persy_rs) for news and announcements, 
join the  [#persy_rs:matrix.org](https://matrix.to/#/!uTwEQyCLprxxbYLvjG:matrix.org?via=matrix.org) for have a chat.


## Contributing

You can find the code on our [Repository](https://gitlab.com/tglman/persy) and report any issue
in our [Issues Tracker](https://gitlab.com/tglman/persy/-/issues)

If you want to have an active part on the Persy development, you can start from reviewing and suggesting API changes, jump directly to hacking the code or just playing a bit with docs.
If you want a list of possibility you can start from the list of [Good First Issue](https://gitlab.com/tglman/persy/-/issues?label_name%5B%5D=Good+First+Issue)
