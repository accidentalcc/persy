# Potential recover Failures

- if a page is used, freed, reused in case of crash if in the journal is still present the free record, 
    it could free the page wrongly wiping the data stored on reuse.
< Can't happen the recovery of pages happen only for not yet finalized transactions, so old free do not have any impact

- if a segment page is freed and in the journal there are still record operations precedent to the free related to that page,
    the recover will fail, because trying to apply the changes to a not valid segment page.
< Can't happen the recovery of pages happen only for not yet finalized transactions, so old operation are not executed do not have any impact

- if a segment page is detached to the segment and freed, and a crash happen before the clean of the operation from the journal,
    the recover could try to re-execute the detach and will fail for wrong values in the page.
< Can't happen the recovery of pages happen only for not yet finalized transactions, so old operation are not executed do not have any impact

- freed segment pages should be recorded in an additional transaction because are computed on second phase, and the original transaction
    may be removed from the journal before the pages are freed.
< yes and done now 

- if the free of segment page is logged should mark witch transaction is related to, to avoid to be executed if the recover of the 
    original transaction is executed.
< Can't happen the recovery of pages happen only for not yet finalized transactions

- if a transaction free segment pages, in case of crash before the transaction that track the free pages is recorded,
    the recover of that transaction should recompute the segment pages to free and free them.
< yes and done now 

- page free done at the clean of a snapshot may leak pages in case of a crash, because the transaction successfully committed will not be 
    recovered so free pages tracked in there will be lost.
< fixed with an additional log entry after commit at the snapshot release to track that resources have been freed.   
